<?php

namespace Tests;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Mobile;
use App\Sms;
use Mockery;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
	public function tearDown(): void
	{
		Mockery::close();
	}

	public function test_it_returns_null_when_name_empty()
	{
		$provider = Mockery::mock(CarrierInterface::class);

		$mobile = new Mobile($provider);
		$this->assertNull($mobile->makeCallByName());
	}

	public function test_it_returns_call_when_data()
	{
		$testCall = new Call(123);
		$provider = Mockery::mock(CarrierInterface::class);

		$provider->shouldReceive('dialContact')
			->once()
			->with(Contact::class)
			->andReturnNull();

		$provider->shouldReceive('makeCall')
			->once()
			->withNoArgs()
			->andReturn($testCall);

		$mobile = new Mobile($provider);
		$returnedCall = $mobile->makeCallByName('Francis');
		$this->assertTrue($returnedCall instanceof Call);
		$this->assertEquals(123, $returnedCall->id);
	}

	public function test_it_returns_call_when_not_found()
	{
		$testCall = new Call(null);
		$provider = Mockery::mock(CarrierInterface::class);
		$provider->shouldReceive('dialContact')
			->once()
			->with(Contact::class)
			->andReturnNull();

		$provider->shouldReceive('makeCall')
			->once()
			->withNoArgs()
			->andReturn($testCall);

		$mobile = new Mobile($provider);
		$returnedCall = $mobile->makeCallByName('Juan');
		$this->assertNull($returnedCall->id);
	}

	public function test_send_sms_fail()
	{
		$testCall = new Call(null);
		$provider = Mockery::mock(CarrierInterface::class);

		$mobile = new Mobile($provider);
		$returnedSms = $mobile->sendSms('+019883012431', 'Welcome to the platform');
		$this->assertNull($returnedSms);
		$this->assertFalse($returnedSms instanceof Sms);
	}

	public function test_send_sms_ok()
	{
		$provider = Mockery::mock(CarrierInterface::class);

		$mobile = new Mobile($provider);
		$returnedSms = $mobile->sendSms('+01988301243', 'Welcome to the platform');

		$this->assertTrue($returnedSms instanceof Sms);
		$this->assertEquals('+01988301243', $returnedSms->phoneNumber);
		$this->assertEquals('Welcome to the platform', $returnedSms->body);
	}
}
