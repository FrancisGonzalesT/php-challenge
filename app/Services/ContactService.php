<?php

namespace App\Services;

use App\Contact;
use App\Sms;


class ContactService
{
	const MAX_PHONE_NUMBER_LEN = 12;

	public static function findByName($name): Contact
	{
		if (in_array(strtolower($name), ['francis', 'angelino'])) {
			return new Contact(123);
		}

		return new Contact();
	}

	public static function validateNumber(string $number): bool
	{
		$filteredPhoneNumber = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
		if (strlen($filteredPhoneNumber) == self::MAX_PHONE_NUMBER_LEN) {
			return true;
		}

		return false;
	}

	public static function sendSms(string $phoneNumber, string $body) : ?Sms
	{
		if (!self::validateNumber($phoneNumber)) {
			return null;
		}

		return new Sms($phoneNumber, $body);
	}
}
